# app.py - a minimal flask api using flask_restful
from flask import * #Flask, flash, render_template, request
from flask_login import login_user , logout_user , current_user , login_required, LoginManager
from werkzeug.security import generate_password_hash, check_password_hash
import socket
import json
import wtforms
from flask_sqlalchemy import SQLAlchemy
import os
import time

#import base64 for encoding bytte string to json
import base64

# Import socket module 
import socket  

app = Flask(__name__)

#golly this needs to be changed
app.secret_key = b'\xb9\x0bmUr\t\x83\x1c\xb7z\xa0\xaa'
#robot_server_pass = b'\x88>\xcc\xb3\xe7N\xc3J\x12=\tFZy\xfa\xda)\xc4\xdbP\xfa\xc4k\xdd'
devel_robot_server_pass = "default key"


#create a login object to abstract that stuff away
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

@app.before_request
def before_request():
    if current_user.is_authenticated:
        g.user = current_user
    else:
        g.user = None

#--------------------------------------------------------------------------
#
#                                Pages
#
#--------------------------------------------------------------------------

#The landing page
@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

#A page that gives robot controls
@app.route('/dashboard', methods=['GET'])
@login_required
def dashboard():
    message = "Hello, Wold"
    return render_template('dashboard.html', message=message)

#A page to veiw your registered robot
@app.route('/your_robot', methods=['GET'])
@login_required
def your_robot():
    pass



#--------------------------------------------------------------------------
#
#                          LOGGINS AND LOGOUTS
#
#--------------------------------------------------------------------------
#Regester an acount -- mostlikely a user wont be able to register themselfs
@app.route('/register' , methods=['GET','POST'])
def register():

    if request.method == 'GET':
        return render_template('register.html')
    else:

        #check for preexisting account
        if User.query.filter_by(username = request.form['username']):
            flash('Username Taken')
            return redirect(url_for('register'))

        elif User.query.filter_by(email = request.form['email']):
            flash('email allready regestered')
            return redirect(url_for('login'))

        #insert new user into database
        user = User(username=request.form['username'] , password=generate_password_hash(request.form['password'], method='sha256'), email=request.form['email'], authenticated=False)

        #add these to a database
        db.session.add(user)
        db.session.commit()

        #check that this is entered successfully
        user_check = User.query.filter_by(username = user.username)
        if not user_check:
            flash('something went wrong')

        #confirm registration
        flash('User successfully registered')
        return redirect(url_for('dashboard'))


#A login page
@app.route('/login',methods=['GET','POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')

    username = request.form['username']
    password = request.form['password']

    registered_user = User.query.filter_by(username=username).first()
    if (registered_user is None) or (not check_password_hash(registered_user.password, password)):
        flash('Username or Password is invalid' , 'error')
        return redirect(url_for('login'))

    login_user(registered_user)
    flash('Logged in successfully')
    return redirect(url_for('dashboard'))


#A route to control loggin out
@app.route('/logout', methods=['GET'])
@login_required
def logout():
    if request.method == 'GET':
        logout_user()
        return redirect('/')


@app.route('/register_robot', methods=['GET', 'POST'])
@login_required
def register_robot():
    if request.method == 'GET':
        return render_template('register_robot.html')

    #check if a user is allowed to regester a robot
    if g.user.username != 'GWSRobotics':
        flash('you do not have permision to regester a robot')
        return redirect('/')

    #check user password
    password = request.form['password']
    if not check_password_hash(g.user.password, password):
        flash('incorrect password')
        return render_template('register_robot.html')

    #use form info to save robot in database
    company_name = request.form['company_name']
    curr_ip = request.form['robot_ip']
    port = 55555


    #check robot connection
    #if not check_robot_conection(curr_ip, port):
    #    flash('could not establish connection, see trouble-shooting.')
    #    return render_template('register_robot.html')

    #should not save to database before regestering on robot?


    #save details to robot database
    #robot = Robot(registered_user_id=g.user.id, owner_name=company_name, last_ip=curr_ip, port=port, authentication_hash=os.urandom(24))
    robot = Robot(registered_user_id=g.user.id, owner_name=company_name, last_ip=curr_ip, port=port, authentication_hash='default_key')
    #add these to a database
    db.session.add(robot)
    db.session.commit()


    #store reference to the regestered robot here
    g.user.robot_id = robot.id
    db.session.commit()
    
    #tell the robot the authentication hash
    if not register_user_on_robot(curr_ip, port, generate_password_hash(robot.authentication_hash), robot.registered_user_id):
        flash('Failed to regester user on robot')
        return render_template('register_robot.html')

    flash('robot id : ' + str(robot.id) + ', user ref : ' + str(g.user.robot_id) + ' user id : ' + str(g.user.id))

    return redirect('/dashboard')


def check_robot_conection(ip, port):
    r = send_to_pepper({
            'command' : 'test connection'
        })
    return True if r else False


def register_user_on_robot(ip, port, hashed_authentication, user_id):
    #returns success of failure
    r = json.loads(send_to_pepper({
            'command' : 'new user',
            'user_id' : 'user_id',
            'authentication_hash' : hashed_authentication
        }))
    return True if r['result'] == 'success' else False


#-----------------------------------------------------------------
#
#                       API for pepper coms
#
#-----------------------------------------------------------------
@app.route('/get_robot_info_short')
@login_required
def short_robot_info():
    #supplies db ifno to the website
    robot = Robot.query.filter_by(registered_user_id=current_user.id).first()
    
    if not robot:
        return json.dumps({
        'is_robot' : False
        })

    return json.dumps({
        'is_robot' : True,
        'owner_name' : robot.owner_name,
        'ip' : robot.last_ip,
        'conection status' : 'not tested'
    })

@app.route('/test_conection')
@login_required
def test_conection():

    #test for now by doing rastas
    return 'your robot is ready to connect' if send_to_pepper({ 'command' : 'rasta', 'time' : 1 }) == None else 'Unable to establish connection to pepper'



@app.route('/say_somthing/<text>')
@login_required
def say_somthing(text):
    print('will try to send_messaged')

    #send request to pepper
    r = send_to_pepper({'command' : 'say', 'text' : text.replace('+', ' ')})

    print('returned from pepper server : ' + r)

    return 'operation : ' + r

    #return r.text

@app.route('/do_rastas/<time>')
@login_required
def rastas(time):
    print('will do rasta lights')

    r = send_to_pepper({'command' : 'rasta', 'time' : time})
    
    print('returned from pepper server : ' + r)

    return 'operation : ' + r


@app.route('/do_dance/')
@login_required
def do_dance():

    print 'i have been asked to dance'

    r = send_to_pepper({'command' : 'do_dance'}) 

    print('returned from pepper server : ' + r)

    return 'doing a random dance'


@app.route('/talkSettings/<setting>/<value>', methods=['GET', 'POST'])
@login_required
def talkSettings(setting, value):

    if request.method == 'POST':
        #user wants to set a setting
        print ('\n\nsetting setting : {}'.format(setting, value))

        r = send_to_pepper({'command' : 'set_setting', 'setting' : setting, 'value' : value})

        print('returned from pepper server : ' + r)

        return r
    
    else:
        #user wants to get information
        print ('\n\n\nGET SETTING REQUEST!!\n\n\n')
        r = send_to_pepper({'command' : 'get_setting', 'setting' : setting})

        print('returned from pepper server : ' + r)

        return r

@app.route('/partyScript/<setting>/<value>', methods=['GET', 'POST'])
@login_required
def partyScript(setting, value):

    if request.method == 'POST':

        
        #user wants to set a setting
        r = send_to_pepper({'command' : 'partyScript', 'setting' : setting, 'value' : value})

        print('returned from pepper server : ' + r)

        return r
    
    else:
        #user wants to get information
        r = send_to_pepper({'command' : 'partyScript_get', 'setting' : setting, 'value' : value})

        print('returned from pepper server : ' + r)

        return 


def send_to_pepper(message):

    '''
    A function that sends a message to the pepper robot.
    accepts jsonifiable objects
    returns a reply string
    '''

    robot = Robot.query.filter_by(id=g.user.robot_id).first()
    if robot == None:
        return 'there is no regestered robot'


    message = wrapMessage(message, robot)

    # Create a socket object 
    s = socket.socket()          
      
    # Define the port on which we want to connect 
    port = 55555

    print 'connecting'
    print 'robot_ip : ' + message['robot_ip']
    print 'port : {}'.format(message['robot_port'])

    # connect to the server on local computer 
    s.connect((message['robot_ip'], message['robot_port'])) #'192.168.0.116'

    print message
    print('connected')
    print(json.dumps(message).encode())

    # receive data from the server 
    s.sendall(json.dumps(message).encode()) 
    # close the connection 

    print 'message sent'

    output = s.recv(1024)

    #send a null message to close conection
    s.send(''.encode())

    #if this not closed, eek!
    s.close()  

    return output

def wrapMessage(message, robot):
    #takes a message to pepper and wraps it in an authenticated protocal 

    return {
        'target_robot_id' : robot.id,
        'user_authentication_hash' : str(robot.authentication_hash),
        'server_authentication' : devel_robot_server_pass, #robot_server_pass,
        'robot_ip' : robot.last_ip,
        'robot_port' : robot.port,
        'time' : time.time(),
        'message' : message,
    }


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))

#--------------------------------------------------------------------------
#
#                               The Model
#
#--------------------------------------------------------------------------

# create the database object
app.config['SQLALCHEMY_ECHO'] = True
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////' + os.path.join(basedir, 'model/test.db')
db = SQLAlchemy(app)

class User(db.Model):
    '''
    This provides default implementations for the methods that Flask-Login
    expects user objects to have.
    '''

    id = db.Column(db.Integer,primary_key=True)
    email = db.Column(db.Unicode(128))
    username = db.Column(db.Unicode(128))
    password = db.Column(db.Unicode(1024))
    authenticated = db.Column(db.Boolean, default=False)
    robot_id = db.Column(db.Integer)
    authentication_hash = db.Column(db.Unicode(1024))


    @property
    def is_active(self):
        return True

    @property
    def is_authenticated(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def __eq__(self, other):
        '''
        Checks the equality of two `UserMixin` objects using `get_id`.
        '''
        if isinstance(other, UserMixin):
            return self.get_id() == other.get_id()
        return NotImplemented

    def __ne__(self, other):
        '''
        Checks the inequality of two `UserMixin` objects using `get_id`.
        '''
        equal = self.__eq__(other)
        if equal is NotImplemented:
            return NotImplemented
        return not equal

class Robot(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    registered_user_id = db.Column(db.Integer)
    owner_name = db.Column(db.Unicode(256))
    last_ip = db.Column(db.Unicode(64))
    port = db.Column(db.Integer)
    authentication_hash=db.Column(db.Unicode(1024))




db.create_all()
db.session.commit()


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')