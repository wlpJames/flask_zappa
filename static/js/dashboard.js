//get robot info on page load
$( document ).ready(function() {
    

	$.ajax({url: "/get_robot_info_short", 
		success: function(result) {
			console.log(result);
			result = JSON.parse(result) 
			if (result.is_robot) {
				document.getElementById('robot_info').style.display = 'block';
				document.getElementById('companyName').innerHTML = result['owner_name'];
				document.getElementById('robot_ip').value = result['ip'];
				document.getElementById('conection-status').innerHTML = result['conection status'];
			}
			else {
				document.getElementById('robot_info').style.display = 'none';	
				document.getElementById('reg_robot').style.display = 'block';	
			}
		}
	});

	//get settings
	//needs condition for robot conected
	get_volume();
	get_speed();
	get_pitch();

	get_PS_sets();//for party scripts

});

function search(e){
	if (event.key == 'Enter') {
		submit_say();
	}
}

function submit_say() {
		
	text = document.getElementById('say').value

	const Http = new XMLHttpRequest();
	const url='/say_somthing/' + text.replace(' ',  '+');
	Http.open("GET", url);
	Http.send();

	Http.onreadystatechange = (e) => {
	  console.log(Http.responseText)
	}

	document.getElementById('say').value = '';

}

function lights() {
  		
	time = 1
	const Http = new XMLHttpRequest();
	const url='/do_rastas/' + time.toString();
	Http.open("GET", url);
	Http.send();

	Http.onreadystatechange = (e) => {
	  console.log(Http.responseText)
	}

}

function dance() {
  		
	const Http = new XMLHttpRequest();
	const url='/do_dance/';
	Http.open("GET", url);
	Http.send();

	Http.onreadystatechange = (e) => {
	  console.log(Http.responseText)
	}

}

function get_volume() {
	$.ajax({url: "/talkSettings/volume/0", method: "GET", success: function(result){
		console.log('volume set to : ' + result)
		document.getElementById('volume').value = result;
  	}});
}
function get_speed() {
	$.ajax({url: "/talkSettings/speed/0", method: "GET", success: function(result){
		console.log('speed set to : ' + result)
		document.getElementById('speed').value = result;
  	}});
}
function get_pitch() {
	$.ajax({url: "/talkSettings/pitch/0", method: "GET", success: function(result){
		console.log('pitch set to : ' + result)
		document.getElementById('pitch').value = result;
  	}});
}
function get_PS_sets() {
	$.ajax({url: "/partyScript/enable/0", method: "GET", success: function(result){
		console.log('partyScript : ' + result)
		$('#partyScriptToggle').bootstrapToggle(( result == true || result == 'true') ? 'on' : 'off' );
  	}});

	$.ajax({url: "/talkSettings/max_time/0", method: "GET", success: function(result){
		console.log('max : ' + result)
		document.getElementById('max_time').value = parseFloat(result);
  	}});
  	$.ajax({url: "/talkSettings/min_time/0", method: "GET", success: function(result){
		console.log('min : ' + result)
		document.getElementById('min_time').value = parseFloat(result);
  	}});
}



//slider events
function set_volume() {
	$.ajax({url: "/talkSettings/volume/" + document.getElementById('volume').value, method: "POST", success: function(result){
		console.log('yes')
  	}});
}

function set_pitch() {
	$.ajax({url: "/talkSettings/pitch/" + document.getElementById('pitch').value, method: "POST", success: function(result){
		console.log('yes')
  	}});
}

function set_speed() {
	$.ajax({url: "/talkSettings/speed/" + document.getElementById('speed').value, method: "POST", success: function(result){
		console.log('yes')
  	}});
}





//max time
function max_time_input() {
	if (document.getElementById('max_time').value <= document.getElementById('min_time').value) {
		document.getElementById('max_time').value = parseInt(document.getElementById('min_time').value) + 1;
	}
	document.getElementById('max_value').innerHTML = document.getElementById('max_time').value;
}

function max_time_change() {

	$.ajax({url: "/partyScript/max_time/" + document.getElementById('max_time').value, method: "POST", success: function(result){
	console.log('yes')
  	}});
}


//min time
function min_time_input() {
	if (document.getElementById('min_time').value >= document.getElementById('max_time').value) {
		document.getElementById('min_time').value = parseInt(document.getElementById('max_time').value) -1;
	}
	document.getElementById('min_value').innerHTML = document.getElementById('min_time').value;
}

function min_time_change() {

	$.ajax({url: "/partyScript/min_time/" + document.getElementById('min_time').value, method: "POST", success: function(result){
    	console.log('none')
  	}});
}

//enable disable
function enable_party_script() {
	$.ajax({type: "POST", url: "/partyScript/enable/" + ($('#partyScriptToggle:checked').val()), method: "POST", success: function(result){
    	console.log('none')
  	}});
}

function enable_greeting() {
	$.ajax({type: "POST", url: "talkSettings/greeting/" + ($('#greeter:checked').val()), method: "POST", success: function(result){
    	console.log('none')
  	}});
}

function enable_AL() {
	$.ajax({type: "POST", url: "talkSettings/AL/" + ($('#AL:checked').val()), method: "POST", success: function(result){
    	console.log('none')
  	}});
}

function enable_convo() {
	$.ajax({type: "POST", url: "talkSettings/convo/" + ($('#convo:checked').val()), method: "POST", success: function(result){
    	console.log('none')
  	}});
}