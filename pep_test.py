import qi
import socket
import argparse
import os
import sys
import json

# Import socket module 
import socket                
  
# Create a socket object 
s = socket.socket()          
  
# Define the port on which you want to connect 
port = 55555

print 'connecting'
# connect to the server on local computer 
s.connect(('192.168.0.116', port)) 

print('connected')

message= json.dumps({'command' : 'say', 'text' : 'message received'})

print message

# receive data from the server 
s.sendall(message.encode()) 
# close the connection 

print 'message sent'

print(s.recv(1024))

#send a null message to close conection
s.send(''.encode())

#if this not closed, eek!
s.close()  